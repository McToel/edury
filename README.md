# edury

Edury is an educational memory used at Uni Tübingen. It is build using Vue 3 and a little bit of python

## Config
Edury is configured though GET params in the URL. These params are not transferred. They stay on the device.

A full example for the URL might be: `https://mctoel.gitlab.io/edury/#/?decks={"deck1":"https://example.com/deck1_b55f84b1.json","deck2":"https://example.com/deck2_b55f84b1.json""}&deck_name=deck1&cards_content_types=[{"front":"Picture","back":"neutral"},{"front":"Sketch","back":"neutral"}]`

Let's break the params down
### decks, required
A key value mapping of the name of a deck and a URL where the deck is hosted.

### deck_name, optional
The name of the deck that should be selected on start

### cards_content_types, optional
The contents that should be shown on the cards.

## Make your own game
Edury is open source and allows you to make your own memory decks. The decks we use are not open source, as we don't have the copyright for many of the pictures we use.

In order to make your own deck, you will need the following:
- A static file server with CORS, for example nginx
- Python 3
- A text editor / IDE to edit the decks. We use VSCode

**Decks** are folders placed in the `decks` directory. A **deck** consists of **pairs**. There can be as many **pairs** as you want, but there should not be more **pairs** than fit on screen.

A **pair** has at least one **side**. For a standard memory configuration, each **pair** would have a neutral **side**, a one with a picture. With Edury it is possible to play memory in different ways. For example, there could be a neutral side and on the other side might be a picture of a fruit or the name of the fruit. In this case, the picture would be one side, the fruit another, and the neutral side the last one.

A **side** can either be an image or a markdown formatted text file.


A deck might for example look like this:
```sh
deck1
├── deck_meta.json
├── 0.png
├── pair1
│   ├── 1.jpg
│   ├── 2.jpg
│   └── 3.md
├── pair2
│   ├── 1.jpg
│   ├── 2.jpg
│   └── 3.md
...
```

The file `deck_meta.json` describes the deck:
```json
{
    "neutral": {
        "content_type": "img",
        "resource_name": "0"
    },
    "contents": {
        "Picture":{
            "content_type": "img",
            "resource_name": "1"
        },
        "Sketch":{
            "content_type": "img",
            "resource_name": "2"
        },
        "Description":{
            "content_type": "text",
            "resource_name": "3"
        }
    }
}
```

`0.png` is the neutral side for all cards.

The file names don't have to be numbers, but for every pair, there must be the same. The name of the sides are defined in the field `resource_name` within `deck_meta.json`.

When your deck is finished, you need to convert it and host it somewhere.

In order to convert decks to JSON put them into `decks/` and run:
```bash
pip install -r requirements.txt # Install requirements. Only do this once
python deck_to_json.py --base_url your_base_url
```

When the convention finished successfully a config string will be printed to the console and the files will be written to `public/`.


## Support
For support, please open an [issue](https://gitlab.com/McToel/edury/-/issues).

## Contributing
Contributions are welcome. If you think something is wrong, missing or could be made better, please open an [issue](https://gitlab.com/McToel/edury/-/issues) to start contributing.
