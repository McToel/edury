import { createApp } from 'vue'
import App from './App.vue'
import { VueShowdownPlugin } from 'vue-showdown'
import VueGtag from 'vue-gtag'
import router from './router/index'

const app = createApp(App).use(router)

// the second parameter of app.use() is optional
app.use(VueShowdownPlugin, {
  // set default flavor of showdown
  flavor: 'github',
  // set default options of showdown (will override the flavor options)
  options: {
    emoji: false
  }
})

app.use(router)
app.use(VueGtag, { bootstrap: false, config: { id: 'G-YJP3CR68ND' } }, router)

app.mount('#app')
