import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import MainMemory from '../views/Main.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'edury',
    component: MainMemory
  },
  {
    path: '/about',
    name: 'AboutEdury',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutEdury.vue')
  },
  {
    path: '/no_deck',
    name: 'No Deck',
    component: () => import(/* webpackChunkName: "no_deck" */ '../views/NoDecks.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
