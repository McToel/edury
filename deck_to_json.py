import os
from PIL import Image
import json
import base64
import io
import xxhash
import warnings
import argparse
# import pytesseract


parser = argparse.ArgumentParser()
parser.add_argument("--base_url", help="The url where you host the decks without trailing slash", dest='base_url', default='')
args = parser.parse_args()

MAX_IMAGE_SIZE = 180*4
MIN_IMAGE_SIZE = 180*2
PICTURE_FILE_FORMATS = (".jpg", ".jpeg", ".png", ".bmp", ".gif")


def image_to_webp(input_path: str, path: str) -> None:
    image = Image.open(input_path)
    if min(image.size) < MIN_IMAGE_SIZE:
        warnings.warn(f'image {input_path} is to small ({min(image.size)} px)')
    if max(image.size) > MAX_IMAGE_SIZE:
        image.thumbnail((MAX_IMAGE_SIZE, MAX_IMAGE_SIZE), Image.ANTIALIAS)
    image = image.convert('RGBA')
    image.save(path, 'webp')


def image_to_data_url(file_buffer: io.BytesIO, ext='webp'):
    prefix = f'data:image/{ext};base64,'
    img = file_buffer.getvalue()
    return prefix + base64.b64encode(img).decode('utf-8')


def find_file(name, file_type, path) -> str:
    for file in os.listdir(path):
        if not '.' in file:
            continue
        if file.startswith(name):
            if file_type == 'img' and file.endswith(PICTURE_FILE_FORMATS):
                return os.path.join(path, file)
            elif file_type == 'text' and file.endswith('.md'):
                return os.path.join(path, file)
    else:
        raise FileNotFoundError(f'File {name} of type {file_type} not found in {path}')


def file_to_str(file_path: str, type: str) -> str:
    if type == 'img':
        file_buffer = io.BytesIO()
        image_to_webp(file_path, file_buffer)
        return image_to_data_url(file_buffer, ext='webp')
    elif type == 'text':
        with open(file_path, 'r') as f:
            return f.read()
    else:
        raise ValueError(f'Type {type} not supported')


def parse_decks():
    decks = {}

    # list all folders in the 'decks' directory
    for deck_name in os.listdir('decks'):
        if '.' in deck_name:
            continue

        print('Parsing deck', deck_name)

        meta = json.load(open(f'decks/{deck_name}/deck_meta.json'))
        neutral = {
            'content_type': meta['neutral']['content_type'],
            'content': file_to_str(find_file(
                name=meta['neutral']['resource_name'],
                file_type=meta['neutral']['content_type'],
                path=f'decks/{deck_name}'
            ) , type=meta['neutral']['content_type'])
        }
        
        output_dict = {}

        # list all folders in DECK_NAME
        pairs = os.listdir(f"decks/{deck_name}")
        for pair in pairs:
            if '.' in pair:
                continue

            output_dict[pair] = {
                'default': neutral
            }
            for file_name in meta['contents']:
                output_dict[pair][file_name] = {
                    'content_type': meta['contents'][file_name]['content_type'],
                    'content': file_to_str(find_file(
                        name=meta['contents'][file_name]['resource_name'],
                        file_type=meta['contents'][file_name]['content_type'],
                        path=f'decks/{deck_name}/{pair}'
                    ) , type=meta['contents'][file_name]['content_type'])
                }

        # save output_dict to json file
        version_hash = xxhash.xxh32(json.dumps(output_dict, separators=(',', ':'))).hexdigest()
        with open(f"public/{deck_name}_{version_hash}.json", "w") as f:
            json.dump(output_dict, f)

        decks[deck_name] = f'{args.base_url}/{deck_name}_{version_hash}.json'
    
    print('The decks have been parsed and saved to public/')
    print('The following can be copied and added as `decks` arguments to the url')
    print('Note: The url of a deck will change as soon as anything in the deck changes')
    print(json.dumps(decks, ensure_ascii=False, separators=(',', ':')))


parse_decks()
                